from django.test import TestCase, Client
from django.urls import resolve
from . import views, models, forms
from django.http import HttpRequest
# Create your tests here.
class Story6UnitTest(TestCase):
############################################
# test url
    def test_home_url_exist(self):
        repsonse = Client().get('/story6/')
        self.assertEqual(repsonse.status_code,200)

    def test_add_url_exist(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        repsonse = Client().get('/story6/add/1')
        self.assertEqual(repsonse.status_code,200)

    def test_new_event_url_exist(self):
        repsonse = Client().get('/story6/new_event/')
        self.assertEqual(repsonse.status_code,200)
##############################################
# views

# test home view
    def test_home_view(self):
        func = resolve('/story6/')
        self.assertEqual(func.func, views.event)

    def test_home_redirection(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        response_post = Client().post('/story6/', {'id':1})
        self.assertEqual(response_post.status_code, 302)

    def test_home_redirect_to_correct_url(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        response_post = Client().post('/story6/',{'id': 1})
        self.assertRedirects(response_post, '/story6/add/1')

# test add participantview
    def test_add_view(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        func = resolve('/story6/add/1')
        self.assertEqual(func.func, views.add)

    def test_add_view_redirection(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        response_post = Client().post('/story6/add/1',{'nama':'Dek Depe'})
        self.assertEqual(response_post.status_code, 302)


    def test_orang_form_add_to_correct_event(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        Client().post('/story6/add/1',{'nama':'Dek Depe'})
        self.assertEqual(models.Event.objects.get(id =1).participants.all().count(), 1)
        self.assertEqual(models.Event.objects.get(id =1).participants.all().first().nama,'Dek Depe')
    
    def test_orang_manage_already_existing_orang(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        new_orang = models.Orang(nama="Dek Depe")
        new_orang.save()
        Client().post('/story6/add/1',{'nama':'Dek Depe'})
        self.assertEqual(models.Event.objects.get(id =1).participants.all().count(), 1)
        self.assertEqual(models.Event.objects.get(id =1).participants.all().first().nama,'Dek Depe')
# test new event view
    def test_new_event_view(self):
        func = resolve('/story6/new_event/')
        self.assertEqual(func.func, views.new_event)

    def test_new_event_add_event(self):
         Client().post('/story6/new_event/',{'nama':'meet n greet Kak Pewe'})
         self.assertEqual(models.Event.objects.all().count(), 1)

    def test_new_event_redirection(self):
        response_post = Client().post('/story6/new_event/',{'nama':'meet n greet Kak Pewe'})
        self.assertEqual(response_post.status_code, 302)

#########################################        
# templates
# test home template
    def test_home_template(self):
        repsonse = Client().get('/story6/')
        self.assertTemplateUsed(repsonse, 'event.html')

    def test_home_no_participant(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        request = HttpRequest()
        html_repsonse = views.event(request).content.decode('utf8')
        self.assertIn('meet n greet Kak Pewe', html_repsonse)
        self.assertIn('belum ada yang join ;-;', html_repsonse)
    
    def test_home_with_participant(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        new_orang = models.Orang(nama="Dek Depe")
        new_orang.save()
        new_event.participants.add(new_orang)
        request = HttpRequest()
        html_repsonse = views.event(request).content.decode('utf8')
        self.assertIn('meet n greet Kak Pewe', html_repsonse)
        self.assertIn('Dek Depe', html_repsonse)
    
# test add template
    def test_add_template(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        repsonse = Client().get('/story6/add/1')
        self.assertTemplateUsed(repsonse, 'add_participant.html')

# test new event template
    def test_new_event_template(self):
        repsonse = Client().get('/story6/new_event/')
        self.assertTemplateUsed(repsonse, 'new_event.html')

#####################################################
# models
# test event model
    def test_event_model_exist(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        count_all_event = models.Event.objects.all().count()
        self.assertEqual(count_all_event,1)

    def test_event_model_fields_work(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        new_orang = models.Orang(nama="Dek Depe")
        new_orang.save()
        new_event.participants.add(new_orang)
        self.assertEqual(new_event.nama,"meet n greet Kak Pewe")
        self.assertEqual(new_event.participants.exists(), True)
        self.assertEqual(str(new_event), new_event.nama)

# test orang model
    def test_orang_model_works(self):
        new_orang = models.Orang(nama='Dek Depe')
        new_orang.save()
        count_all_orang = models.Orang.objects.all().count()
        self.assertEqual(count_all_orang, 1)

    def test_orang_model_field_works(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        new_orang = models.Orang(nama="Dek Depe")
        new_orang.save()
        new_event.participants.add(new_orang)
        self.assertEqual(new_orang.nama, 'Dek Depe')
        self.assertEqual(new_orang.event_set.exists(),True)
        self.assertEqual(str(new_orang), new_orang.nama)

#############################################
# test forms
# test Orang form
    def test_orang_form_reject_blank(self):
        form = forms.OrangForm(data={'nama':''})
        self.assertFalse(form.is_valid())

        

    
# test Event form
    def test_event_form_reject_blank(self):
        form = forms.EventForm(data={'nama':''})
        self.assertFalse(form.is_valid())

    def test_event_form_reject_duplicate(self):
        new_event = models.Event(nama="meet n greet Kak Pewe")
        new_event.save()
        form = forms.EventForm(data={'nama':'meet n greet Kak Pewe'})
        self.assertFalse(form.is_valid())