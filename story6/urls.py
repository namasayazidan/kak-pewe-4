from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.event, name='home'),
    path('add/<int:id_number>', views.add, name='add'),
    path('new_event/',views.new_event, name ='add_event')

]
