from django.shortcuts import render, redirect, reverse
from django.http import HttpResponseRedirect
from . import forms
from . import models

# Create your views here.
def event(request):
    response ={
        'event_list':models.Event.objects.all()
    }
    if request.method == 'POST':
        id_number = request.POST.get("id")
        return redirect ('add/'+ id_number)
    return render(request, "event.html", response)

def add(request, id_number):

    event = models.Event.objects.get(id = id_number)
    form = forms.OrangForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        nama = request.POST.get('nama')
        orang = (models.Orang.objects.filter(nama = nama).first() or None)
        if orang:
            event.participants.add(orang)
        else:
            new_orang = models.Orang(nama = nama)
            new_orang.save()
            event.participants.add(new_orang)
        return redirect('/story6/')
    else:
        response ={
            'form':form  
    }
        return render(request, "add_participant.html", response)

def new_event(request):
    form = forms.EventForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        nama = request.POST.get('nama')
        new_event = models.Event(nama = nama)
        new_event.save()
        return redirect('/story6/')
    response ={
        'form':form
    }
    return render(request, "new_event.html", response)

