from django.db import models

# Create your models here.

class Orang(models.Model):
    nama = models.CharField(max_length = 30)
    def __str__(self):
        return self.nama
class Event(models.Model):
    nama = models.CharField(max_length =30)
    participants = models.ManyToManyField(Orang)

    def __str__(self):
        return self.nama