from django.forms import ModelForm,Form
from .models import Event, Orang
from django import forms

class OrangForm(ModelForm):
    class Meta:
        model = Orang
        fields =[
            'nama'
            ]
        labels = {
            'nama':'Masukan namamu'
        }

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields =['nama']
        label={
            'nama':'Masukan nama acara'
        }
    
    def clean(self):
        super(EventForm, self).clean()
        nama = self.cleaned_data.get('nama')
        for event in Event.objects.all():
            if event.nama == nama:
                self._errors['nama'] = self.error_class(['nama event sudah diambil.... pilih yang baru ya...'])
                return self.cleaned_data
        return self.cleaned_data