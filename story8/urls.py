from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.index, name='index'),
    path('search/<str:data>', views.search, name='search'),
    path('search/', views.search, name='search'),
]
