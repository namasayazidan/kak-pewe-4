from django.test import TestCase, Client
from django.urls import resolve
import requests
from unittest import skip
from . import views
# Create your tests here.
class Story8UnitTest(TestCase):
############################
# test urls
    def test_index_url(self):
        repsonse = Client().get('/story8/')
        self.assertEqual(repsonse.status_code,200)

    def test_search_url(self):
        response = Client().get('/story8/search/ayam')
        self.assertEqual(response.status_code, 200)

######################################
# test view
    def test_index_view(self):
        func = resolve('/story8/')
        self.assertEqual(func.func, views.index)

    def test_search_view(self):
        func = resolve('/story8/search/data')
        self.assertEqual(func.func, views.search)
    @skip("agar tidak rng")
    def test_search_response(self):
        response = Client().get('/story8/search/data')
        json_file = requests.get('https://www.googleapis.com/books/v1/volumes?q=data').json()
        self.maxDiff = None
        self.assertEqual(response.json(), json_file)

#########################################        
# templates
# test index template
    def test_index_template(self):
        repsonse = Client().get('/story8/')
        self.assertTemplateUsed(repsonse, 'index_story8.html')


