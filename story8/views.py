from django.shortcuts import render
import requests
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def index(request):
    return render(request, "index_story8.html")


def search(request, data):
    return JsonResponse(requests.get('https://www.googleapis.com/books/v1/volumes?q='+ data).json())