from django.urls import path, re_path

from . import views

app_name = 'story4'

urlpatterns = [
    path('schedule', views.schedule, name='schedule'),
    path('add/', views.add, name='add'),
    path('manager/', views.manager, name='manager'),
    path('', views.mata_kuliah, name='matkul'),
    path('add_matkul', views.add_matkul,name='add_matkul'),
    path('detil_matkul/<int:item_id>', views.detil_matkul, name="detil_matkul")

]
