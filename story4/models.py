from django.db import models
from datetime import time
from math import ceil,floor
class Kelas(models.Model):
    HARI =[ (0, "Senin"),
            (1, "Selasa"),
            (2, "Rabu"),
            (3, "Kamis"),
            (4, "Jumat"),
            (6, "Sabtu")
            ]

    nama = models.CharField(max_length=30)
    hari = models.IntegerField(choices=HARI, default="Senin")
    start_time = models.TimeField()
    end_time = models.TimeField()
    bcolor = models.CharField(max_length=30)
    tcolor = models.CharField(max_length=30)

    def calc_length(self, interval):
        total = total_minute(self.end_time)-total_minute(self.start_time)
        return ceil(total/interval)+1

    def calc_period(self, interval, day_start):
        total = total_minute(self.start_time)-total_minute(day_start)
        return floor(total/interval)


class MataKuliah(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks =  models.CharField(max_length=30)
    deskripsi = models.TextField(default= "monggo diisi")
    semester = models.CharField(max_length=30)
    ruang = models.CharField(max_length=30)

#####################
# utility functions
def total_minute(time):
    return 60*time.hour+time.minute