from django.shortcuts import render, redirect
from datetime import datetime, timedelta, time, date
from . import models
from .forms import KelasForm, MatkulForm
start_time = "08:00"
end_time = "18:30"
interval = 30



# Create your views here.
def schedule(request):
    kelas_list = models.Kelas.objects.all()
    response={
            "schedule_table":create_event_table(kelas_list,interval,start_time,end_time)
    }           
    return render(request, 'table.html', response)

def add(request):
    form = KelasForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return render(request, 'add_new.html', {"form": form, "status":"Berhasil menambahkan kelas baru, silahkan cek Scheduler ^^"})
    else:
        return render(request, 'add_new.html', {"form": form, "status":""})
    
    
def manager(request):
    response = {
        'kelas_list':create_kelas_list(models.Kelas.objects.all())
    }
    
    id_number = request.POST.get("id")
    
    if "delete" in request.POST and  request.method == 'POST':
        kelas_instance = models.Kelas.objects.get(id=id_number)
        kelas_instance.delete()
        response = {
        'kelas_list':create_kelas_list(models.Kelas.objects.all())
        } 
        return render(request, "manager.html", response)  
    return render(request, "manager.html", response)

def mata_kuliah(request):
    mata_kuliah_list=models.MataKuliah.objects.all()
    response = {
        "matkul_list":mata_kuliah_list
    }
    id_number = request.POST.get("id")
    
    if "delete" in request.POST:
        matkul_instance = models.MataKuliah.objects.get(id=id_number)
        matkul_instance.delete()
        response = {
        'matkul_list':models.MataKuliah.objects.all()
        } 
    if "detail" in request.POST:
        return redirect('detil_matkul/'+id_number)
    return render(request, "mata_kuliah.html", response)

def add_matkul(request):
    form = MatkulForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return render(request, 'add_matkul.html', {"form": form, "status":"Berhasil menambahkan matkul baru, silahkan cek halaman Story 4 ^^"})
    else:
        return render(request, 'add_matkul.html', {"form": form, "status":""})


def detil_matkul(request, item_id):
    matkul_instance = models.MataKuliah.objects.get(id=item_id)
    return render(request, 'detil_matkul.html',{'matkul':matkul_instance})

##########################################################################


# Utility functions
def  create_time_table(start_time, end_time,interval):
    current = datetime.combine(date.min,time.fromisoformat(start_time))
    end = datetime.combine(date.min,time.fromisoformat(end_time))
    time_list =[current.strftime("%H:%M")]
    while current < end:
        current += timedelta(minutes=interval)
        time_list.append(current.strftime("%H:%M"))
    return time_list

def create_event_table(kelas_list, interval, start_time,end_time):
    time_list = create_time_table(start_time,end_time,interval)
    schedule_table = []
    for i in range(len(time_list)):
        schedule_table.append([{"row":1,"content":time_list[i]}])
        for j in range(6):
            schedule_table[i].append({"row":1,"content":""})
    for kelas in kelas_list:
        schedule_table[kelas.calc_period(interval,time.fromisoformat(start_time))][kelas.hari+1]=(
            {"row":kelas.calc_length(interval),"content":  kelas.nama, "bcolor":kelas.bcolor,
            "tcolor":kelas.tcolor,})
    return schedule_table

def create_kelas_list(kelas_list):
    HARI ={ 
        0: "Senin",
        1: "Selasa",
        2: "Rabu",
        3: "Kamis",
        4: "Jumat",
        6: "Sabtu"
    }
    readable_list = []
    for kelas in kelas_list:
        kelas_dict ={}
        kelas_dict["nama"] = kelas.nama
        kelas_dict["hari"] = HARI[kelas.hari]
        kelas_dict["start_time"] =kelas.start_time.strftime("%H:%M")
        kelas_dict["end_time"] =kelas.end_time.strftime("%H:%M")
        kelas_dict["bcolor"] = kelas.bcolor
        kelas_dict["tcolor"] = kelas.tcolor
        kelas_dict["id"] = kelas.id
        readable_list.append(kelas_dict)
    return readable_list