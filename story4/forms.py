from django.forms import ModelForm,Form
from .models import Kelas, MataKuliah
from django import forms
class KelasForm(ModelForm):
    class Meta:
        model = Kelas
        fields = ['nama','hari','start_time', 'end_time', 'bcolor','tcolor']
        labels = {
            "nama": "Nama Kelas",
            "start_time":"Awal Kelas",
            "end_time":"Akhir Kelas",
            "bcolor":"Warna Background Kelas",
            "hari":"hari",
            "tcolor":"warna text",
        }
      
    def clean(self):
        super(KelasForm,self).clean()

        start_time = self.cleaned_data.get('start_time')
        end_time = self.cleaned_data.get('end_time')
        hari = self.cleaned_data.get('hari')
        if(start_time == None):
            self._errors['start_time'] = self.error_class(['format waktu tidak valid, gunakan HH:MM'])
            return self.cleaned_data
        if (end_time == None):
            self._errors['end_time'] = self.error_class(['format waktu tidak valid, gunakan HH:MM'])
            return self.cleaned_data
        minute_start =total_minute(start_time)
        minute_end = total_minute(end_time)
        if total_minute(end_time)<total_minute(start_time):
            self._errors['start_time'] = self.error_class(['waktu akhir lebih dulu dari waktu awal'])
            self._errors['end_time'] = self.error_class(['waktu akhir lebih dulu dari waktu awal'])
            return self.cleaned_data
        
        for kelas in Kelas.objects.all():
            if kelas.hari == hari and (total_minute(kelas.end_time) >= minute_start and total_minute(kelas.start_time)<=minute_end):                    
                self._errors['start_time'] = self.error_class(['ada overlap kelas'])
                self._errors['end_time'] = self.error_class(['ada overlap kelas'])
                return self.cleaned_data
        return self.cleaned_data
        

class MatkulForm(ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama', 'dosen', 'sks', 'semester', 'ruang', 'deskripsi'
        ]
        labels = {
            'nama':'nama Mata Kuliah',
            'dosen':'Dosen Pengajar',
            'sks':'Jumlah SKS',
            'semester':'Semester',
            'ruang':'Ruang',
            'deskripsi':'Deskripsi kelas',
        }


###############################################
#utility functions
def total_minute(time):
    return time.hour*60+time.minute