from django.test import TestCase, Client
from django.urls import resolve
from . import views
# Create your tests here.
class Story7UnitTest(TestCase):
############################
# test urls
    def test_index_url(self):
        repsonse = Client().get('/story7/')
        self.assertEqual(repsonse.status_code,200)


######################################
# test view
    def test_index_view(self):
        func = resolve('/story7/')
        self.assertEqual(func.func, views.index)

#########################################        
# templates
# test index template
    def test_index_template(self):
        repsonse = Client().get('/story7/')
        self.assertTemplateUsed(repsonse, 'index.html')