from django.urls import path

from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutMe/', views.aboutMe, name='about me'),
    path('projects/', views.projects, name='projects'),
    path('experience/',views.experience, name='experience'),
    path('contacts/', views.contacts, name='contacts')
]
