from django.shortcuts import render
from . import models
name = "Priscila Wening"
motto = "Programming, Web Design, Game Design, all in one"

contact = '''
email:priscila.wening@gmail.com
phone:0812181000523
reference:by email
'''
# Create your views here.
def home(request):
    response = {"name":name,
                "motto": motto}
    return render(request,'home.html', response)

def aboutMe(request):
    info_list =[]
    for info in models.Profil.objects.all():
        info_list.append([info.title, info.content])

    response = {"info":info_list}
    return render(request, 'block content.html', response)

def projects(request):
    info_list =[]
    for info in models.Projects.objects.all():
        info_list.append([info.title, info.content])


    response = {"info":info_list}
    return render(request, 'block content.html', response)

def experience(request):
    info_list =[]
    for info in models.Experience.objects.all():
        info_list.append([info.title, info.content])


    response = {"info":info_list}
    return render(request, 'block content.html', response)

def contacts(request):
    response = {"contact": contact}
    return render(request, 'contacts.html', response)