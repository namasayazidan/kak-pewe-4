from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = "Priscila Wening" # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000,7,7) #TODO Implement this, format (Year, Month, Date)
npm = 18073027370 # TODO Implement this
hobby = "Web Design, Organizations, Desigining in general"
description = '''
"A fictional Character made by the PPW Tutor Team to serve as a mascot for the class.
Known for her energy and bright personality, the PPW Tutor Team chose her so that their students can be happy as  'Kak Pewe' gives them assignments"
'''
image = "https://i.imgur.com/BGkEvBd.png"

table = [["2000", "Fictional year of birth"],
        ["2006-2012", "Fictional Elementary School"],
        ["2012-2015", "Fictional Junior High School"],
        ["2015-2018", "Fictional High School"],
        ["2018-current", "CSUI"],
        ["2020", "Currently acting as the mascot of PPW class"],
        ["2020", "Created this website so that people can get to know me better"]]
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
     "hobby":hobby, "description":description, "table_content": create_table(table),
     "image_path": image}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

def create_table(table_list):
    table_content = ""
    for entry in table_list:
        table_content +="<tr><td>" + entry[0] + "</td><td>" + entry[1] + "</td></tr>"
    return table_content