from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

from . import forms

def signup(request):
    if request.method == 'POST':
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('story9:index')
    else:
        form = forms.SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def index(request):
    return render(request, 'registration/index_story9.html')