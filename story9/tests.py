from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views
from django.http import HttpRequest
import requests

from . import forms


from . import views
# Create your tests here.
class Story9UnitTest(TestCase):
############################
# test urls

    def test_index_url(self):
        repsonse = Client().get('/story9/')
        self.assertEqual(repsonse.status_code,200)
    def test_login_url(self):
        repsonse = Client().get('/story9/login/')
        self.assertEqual(repsonse.status_code,200)
    def test_signup_url(self):
        repsonse = Client().get('/story9/signup/')
        self.assertEqual(repsonse.status_code,200)
    def test_sign_up_redirect(self):
        response = Client().post('/story9/signup/',{'username':'john', 'password1':'gaktauah1', 'password2':'gaktauah1', 'email':'lennon@thebeatles.com'})
        self.assertRedirects(response, '/story9/')
    
    def test_logout_url(self):
        repsonse = Client().get('/story9/logout/')
        self.assertEqual(repsonse.status_code,302)
    
######################################
# test view
    def test_index_view(self):
        func = resolve('/story9/')
        self.assertEqual(func.func, views.index)
    def test_login_view(self):
        func = resolve('/story9/login/')
        self.assertEqual(func.func.__name__, auth_views.LoginView.as_view().__name__)
    def test_signup_view(self):
        func = resolve('/story9/signup/')
        self.assertEqual(func.func, views.signup)
    def test_logout_view(self):
        func = resolve('/story9/logout/')
        self.assertEqual(func.func.__name__, auth_views.LogoutView.as_view().__name__)
   

    
    

   
#########################################        
# templates
# test index template
    def test_index_template(self):
        repsonse = Client().get('/story9/')
        self.assertTemplateUsed(repsonse, 'registration/index_story9.html')

    def test_index_logged_in(self):
        User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        response = Client().post('/story9/login/',{'username':'john', 'password':'johnpassword'})
        request = response.wsgi_request
        html_repsonse = views.index(request).content.decode('utf8')
        self.assertIn('selamat datang john', html_repsonse)

    def test_index_logged_of(self):
        request = HttpRequest()
        html_repsonse = views.index(request).content.decode('utf8')
        self.assertIn('silahkan log in atau sign up', html_repsonse)
#test login template
    def test_login_template(self):
        repsonse = Client().get('/story9/login/')
        self.assertTemplateUsed(repsonse, 'registration/login.html')

    def test_signup_template(self):
        repsonse = Client().get('/story9/signup/')
        self.assertTemplateUsed(repsonse, 'registration/signup.html')



###############################
#forms
    
    def test_signup_form(self):
        form = forms.SignUpForm(data={'username':'john', 'password1':'gaktauah1', 'password2':'gaktauah1', 'email':'lennon@thebeatles.com'})
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(User.objects.all().count(), 1)

    
    def test_login_form_correct_pass(self):
        User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        response = Client().post('/story9/login/',{'username':'john', 'password':'johnpassword'})
        self.assertTrue(response.wsgi_request.user.is_authenticated)
    
    def test_login_form_wrong_pass(self):
        User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        response = Client().post('/story9/login/',{'username':'john', 'password':'eheheheheh'})
        self.assertFalse(response.wsgi_request.user.is_authenticated)

    def test_signup_form_user_exist(self):
        User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        form = forms.SignUpForm(data={'username':'john', 'password1':'gaktauah1', 'password2':'gaktauah1', 'email':'lennon@thebeatles.com'})
        self.assertFalse(form.is_valid())

